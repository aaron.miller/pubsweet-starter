variables:
  KANIKO_CACHE_ARGS: "--cache=true --cache-copy-layers=true --cache-ttl=24h"
  VERSIONLABELMETHOD: "OnlyIfThisCommitHasVersion" # options: "OnlyIfThisCommitHasVersion","LastVersionTagInGit"
  IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  IMAGE_LABELS: >
    --label org.opencontainers.image.vendor=$CI_SERVER_URL/$GITLAB_USER_LOGIN
    --label org.opencontainers.image.authors=$CI_SERVER_URL/$GITLAB_USER_LOGIN
    --label org.opencontainers.image.revision=$CI_COMMIT_SHA
    --label org.opencontainers.image.source=$CI_PROJECT_URL
    --label org.opencontainers.image.documentation=$CI_PROJECT_URL
    --label org.opencontainers.image.licenses=$CI_PROJECT_URL
    --label org.opencontainers.image.url=$CI_PROJECT_URL
    --label vcs-url=$CI_PROJECT_URL
    --label com.gitlab.ci.user=$CI_SERVER_URL/$GITLAB_USER_LOGIN
    --label com.gitlab.ci.email=$GITLAB_USER_EMAIL
    --label com.gitlab.ci.tagorbranch=$CI_COMMIT_REF_SLUG
    --label com.gitlab.ci.pipelineurl=$CI_PIPELINE_URL
    --label com.gitlab.ci.commiturl=$CI_PROJECT_URL/commit/$CI_COMMIT_SHA
    --label com.gitlab.ci.cijoburl=$CI_JOB_URL
    --label com.gitlab.ci.mrurl=$CI_PROJECT_URL/-/merge_requests/$CI_MERGE_REQUEST_ID

stages:
  - .pre
  - build
  - check
  - test

get-latest-git-version:
  stage: .pre
  image: 
    name: alpine/git
    entrypoint: [""]
  rules:
    - if: '$VERSIONLABELMETHOD == "LastVersionTagInGit"'    
  script:
    - |
      echo "the google kaniko container does not have git and does not have a packge manager to install it"
      git clone https://github.com/GoogleContainerTools/kaniko.git
      cd kaniko
      echo "$(git describe --abbrev=0 --tags)" > ../VERSIONTAG.txt
      echo "VERSIONTAG.txt contains $(cat ../VERSIONTAG.txt)"
  artifacts:
    paths:
      - VERSIONTAG.txt

build:
  stage: build
  image: 
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - | 
      echo "Building and shipping image to $CI_REGISTRY_IMAGE"
      #Build date for opencontainers
      BUILDDATE="'$(date '+%FT%T%z' | sed -E -n 's/(\+[0-9]{2})([0-9]{2})$/\1:\2/p')'" #rfc 3339 date
      IMAGE_LABELS="$IMAGE_LABELS --label org.opencontainers.image.created=$BUILDDATE --label build-date=$BUILDDATE"
      #Description for opencontainers
      BUILDTITLE=$(echo $CI_PROJECT_TITLE | tr " " "_")
      IMAGE_LABELS="$IMAGE_LABELS --label org.opencontainers.image.title=$BUILDTITLE --label org.opencontainers.image.description=$BUILDTITLE"
      #Add ref.name for opencontainers
      IMAGE_LABELS="$IMAGE_LABELS --label org.opencontainers.image.ref.name=$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG"

      #Build Version Label and Tag from git tag, LastVersionTagInGit was placed by a previous job artifact
      if [[ "$VERSIONLABELMETHOD" == "LastVersionTagInGit" ]]; then VERSIONLABEL=$(cat VERSIONTAG.txt); fi
      if [[ "$VERSIONLABELMETHOD" == "OnlyIfThisCommitHasVersion" ]]; then VERSIONLABEL=$CI_COMMIT_TAG; fi
      if [[ ! -z "$VERSIONLABEL" ]]; then 
        IMAGE_LABELS="$IMAGE_LABELS --label org.opencontainers.image.version=$VERSIONLABEL"
        ADDITIONALTAGLIST="$ADDITIONALTAGLIST $VERSIONLABEL"
      fi
      
      ADDITIONALTAGLIST="$ADDITIONALTAGLIST $CI_COMMIT_REF_SLUG $CI_COMMIT_SHORT_SHA"
      if [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]]; then ADDITIONALTAGLIST="$ADDITIONALTAGLIST latest"; fi
      if [[ -n "$ADDITIONALTAGLIST" ]]; then 
        for TAG in $ADDITIONALTAGLIST; do 
          FORMATTEDTAGLIST="${FORMATTEDTAGLIST} --tag $CI_REGISTRY_IMAGE:$TAG "; 
        done; 
      fi
      
      #Reformat Docker tags to kaniko's --destination argument:
      FORMATTEDTAGLIST=$(echo "${FORMATTEDTAGLIST}" | sed s/\-\-tag/\-\-destination/g) 

      echo "Kaniko arguments to run: --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile $KANIKO_CACHE_ARGS $FORMATTEDTAGLIST $IMAGE_LABELS"
      mkdir -p /kaniko/.docker
      echo "{\"auths\":{\"$CI_REGISTRY\":{\"auth\":\"$(echo -n $CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD | base64)\"}}}" > /kaniko/.docker/config.json
      /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile $KANIKO_CACHE_ARGS $FORMATTEDTAGLIST $IMAGE_LABELS

lint:
  image: $IMAGE_TAG
  stage: check
  variables:
    GIT_STRATEGY: none
  script:
    - cd ${HOME}
    - yarn lint

# test:chrome:
#   image: $IMAGE_TAG
#   stage: test
#   variables:
#     GIT_STRATEGY: none
#     # setup data for postgres image
#     POSTGRES_USER: test
#     POSTGRES_PASSWORD: pw
#     # connection details for tests
#     PGUSER: test
#     PGPASSWORD: pw
#     NODE_ENV: test
#   services:
#     - postgres
#   script:
#     - cd ${HOME}
#     # specify host here else it confuses the linked postgres image
#     - PGHOST=postgres yarn testcafe 'chrome:headless --no-sandbox' test/**/*.test.js

# test:firefox:
#   image: $IMAGE_TAG
#   stage: test
#   variables:
#     GIT_STRATEGY: none
#     # setup data for postgres image
#     POSTGRES_USER: test
#     POSTGRES_PASSWORD: pw
#     # connection details for tests
#     PGUSER: test
#     PGPASSWORD: pw
#     NODE_ENV: test
#   services:
#     - postgres
#   script:
#     - cd ${HOME}
#     # specify host here else it confuses the linked postgres image
#     - PGHOST=postgres yarn testcafe firefox:headless test/**/*.test.js
