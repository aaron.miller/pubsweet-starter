FROM docker.io/node:erbium-alpine3.15

# Install prerequisites and helper packages
RUN apk add --no-cache --upgrade \
	bash curl git g++ make libc6-compat py3-pip

# Download and install glibc
# ENV GLIBC_VERSION 2.35-r1
# RUN curl -Lo /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub && \
#   curl -Lo glibc.apk "https://github.com/sgerrand/alpine-pkg-glibc/releases/download/${GLIBC_VERSION}/glibc-${GLIBC_VERSION}.apk" && \
#   curl -Lo glibc-bin.apk "https://github.com/sgerrand/alpine-pkg-glibc/releases/download/${GLIBC_VERSION}/glibc-bin-${GLIBC_VERSION}.apk" && \
#   apk add --force-overwrite glibc-bin.apk glibc.apk && \
#   /usr/glibc-compat/sbin/ldconfig /lib /usr/glibc-compat/lib && \
#   echo 'hosts: files mdns4_minimal [NOTFOUND=return] dns mdns4' >> /etc/nsswitch.conf && \
#   rm -rf glibc.apk glibc-bin.apk /var/cache/apk/*


# Setup python2
# This hack is widely applied to avoid python printing issues in docker containers.
# See: https://github.com/Docker-Hub-frolvlad/docker-alpine-python3/pull/13
ENV PYTHONUNBUFFERED=1
RUN apk add --no-cache python2 && \
    python -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    pip install --upgrade pip setuptools && \
    rm -r /root/.cache


# Installs latest Chromium/FireFox package.
RUN apk upgrade --no-cache --available \
    && apk add --no-cache firefox \
    && apk add --no-cache \
      --repository=https://dl-cdn.alpinelinux.org/alpine/edge/main \
      --repository=https://dl-cdn.alpinelinux.org/alpine/edge/community \
      chromium \
    && apk add --no-cache \
      ttf-freefont \
      font-noto-emoji \
    && apk add --no-cache \
      --repository=https://dl-cdn.alpinelinux.org/alpine/edge/testing \
      wqy-zenhei

ENV CHROME_BIN /usr/bin/chromium-browser
ENV CHROME_PATH /usr/lib/chromium/
ENV NODE_ENV development
ENV PORT 3000
ENV HOME "/home/pubsweet"

RUN mkdir -p ${HOME}
WORKDIR ${HOME}
RUN chown -R node:node .
USER node

COPY package.json yarn.lock ./
COPY .eslintignore .eslintrc .prettierrc ./
COPY public public
COPY app app
COPY scripts scripts
COPY config config
COPY server server
COPY test test
COPY webpack webpack

RUN yarn install --frozen-lockfile  \
  # Remove cache and offline mirror
  && yarn cache clean \
  && rm -rf /npm-packages-offline-cache \
  && yarn build

EXPOSE ${PORT}

CMD []
